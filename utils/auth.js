const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");
const User = require("../models/user");
const { SECRET, SESSION } = require("../config");

/**
 * Create New User
 * Roles = "client", "employee", "supervisor"
 * @param {*} userDetails 
 * @param {*} role 
 * @param {*} res 
 * @returns 
 */
const userRegister = async (userDetails, role, res) => {
    try {
        // Validate the username
        let usernameNotTaken = await validateUsername(userDetails.username);
        if (!usernameNotTaken) {
            return res.status(400).json({
                message: `Username is already taken.`,
                success: false
            });
        }

        // validate the email
        let emailNotRegistered = await validateEmail(userDetails.email);
        if (!emailNotRegistered) {
            return res.status(400).json({
                message: `Email is already registered.`,
                success: false
            });
        }

        // Hash USer Password
        const password = await bcrypt.hash(userDetails.password, 12);
        // create a new user
        const newUser = new User({
            ...userDetails,
            password,
            role
        });

        await newUser.save();
        return res.status(201).json({
            message: "User registration successful",
            success: true
        });
    } catch (err) {
        return res.status(500).json({
            message: `Unable to create your account. ${err}`,
            success: false
        });
    }
};

/**
 * Log in User
 * @param {*} userDetails 
 * @param {*} role 
 * @param {*} res 
 * @returns 
 */
const userLogin = async (userDetails, role, res) => {
    let { username, password } = userDetails;
    // Check if user already exists
    const user = await User.findOne({ username });
    if (!user) {
        return res.status(404).json({
            message: "Username is not found. Invalid login credentials.",
            success: false
        });
    }
    // We will check the role
    if (user.role !== role) {
        return res.status(403).json({
            message: "Please make sure you are logging in from the right portal.",
            success: false
        });
    }

    // Validate is user password is correct
    let isMatch = await bcrypt.compare(password, user.password);
    if (isMatch) {
        // Genetrate Authorization token for User Session
        let token = jwt.sign(
            {
                user_id: user._id,
                role: user.role,
                username: user.username,
                email: user.email
            },
            SECRET,
            { expiresIn: `${SESSION} days` }
        );

        let result = {
            username: user.username,
            role: user.role,
            email: user.email,
            token: `Bearer ${token}`,
            expiresIn: 168
        };

        return res.status(200).json({
            ...result,
            message: "Login Successful",
            sessionExpiry: `${SESSION} days`,
            token: token,
            success: true
        });
    } else {
        return res.status(403).json({
            message: "Incorrect password.",
            success: false
        });
    }
};

/**
 * Validate User Name
 * @param {*} username 
 * @returns 
 */
const validateUsername = async username => {
    let user = await User.findOne({ username });
    return user ? false : true;
};

/**
 * Validate User Email
 * @param {*} email 
 * @returns 
 */
const validateEmail = async email => {
    let user = await User.findOne({ email });
    return user ? false : true;
};

/**
 * Validate user Role
 * @param {*} roles 
 * @returns 
 */
const checkRole = roles => (req, res, next) =>
    !roles.includes(req.user.role)
        ? res.status(401).json("Unauthorized")
        : next();

/**
 * Serialize User Details
 * @param {'*'} user 
 * @returns 
 */
const serializeUser = user => {
    return {
        username: user.username,
        email: user.email,
        name: user.name,
        _id: user._id,
        updatedAt: user.updatedAt,
        createdAt: user.createdAt
    };
};

/**
 * Export Authentication
 */
const userAuth = passport.authenticate("jwt", { session: false });

module.exports = {
    userAuth,
    checkRole,
    userLogin,
    userRegister,
    serializeUser
};