const cors = require("cors");
const exp = require("express");
const bp = require('body-parser');
const { connect } = require('mongoose');
const { success, error } = require("consola");
const passport = require("passport");

//Adding App Constants
const { DB, PORT } = require("./config");

// Initializng application
const app = exp();

// Middlewares
app.use(cors());
app.use(bp.json());
app.use(passport.initialize());

require("./utils/passport")(passport);
app.use("/api", require("./routes/users"));

const startApp = async () => {
    try {
        //Connect with DB
        await connect(DB, {
            useFindAndModify: true,
            useUnifiedTopology: true,
            useNewUrlParser: true
        });

        success({
            message: `Successfully connected with Database \n ${DB}`,
            badge: true
        })

        //Start Listening for Server on Port
        app.listen(PORT, () =>
            success({ message: `Server started on PORT ${PORT}`, badge: true })
        );

    } catch (err) {
        error({
            message: `Unable to connect with DB \n ${err}`,
            badge: true
        })
        
        //Try Reconnecting to DB
        startApp();
    }

};

startApp();
