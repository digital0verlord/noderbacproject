# Question 1
1. Project Requirements
    - Arduino based Mains Failure Detector (For detecting power trip off and on at Feeder and Consumer Nodes and notifying neccessary services using ESP8266)
    - Arduino based Energy Meter (For detecting power inflow and consumption and updating neccessary services using ESP8266)
    - Platform API (For consolidating all IOT systems with User Management and Platform DB)
    - Energy Management Portal (For streaming Energy data from API and IOT Microcontrollers)
    - Mobile Application (Android, iOS)
    - Payment Managements System (Payment Processor & Payment gateway intergration with Cash Inflow and Outflow Analysis with respect of Kw/h)
    
2. Required Services:
    - Authentication Service    (Signup | Login | Password Reset)
    - User Management Service   (Edit Profile | Power Consumption History | User preferences)
    - Payment Service           (Make Payment | Active Subscriptions | Transaction History)
    - IOT Service               (IOT Devices Manager | MQTT Integration)
    - Machine Learning Service   
    - Backup and Recovery Service
    
3. Architecture
    - ![high level architecture](https://drive.google.com/file/d/1ZD5IAfFo5Ux-tMVOfm493-MiBhYQt25K/view?usp=sharing)
    - ![platform architecture](https://drive.google.com/file/d/1nGNHYLRH9KmxhBGbUG5aAPaViuggC1eQ/view?usp=sharing)
  Please right click and open image in new tab to preview.

# Question 2 
1. SUPERVISOR
    - Manages product categories
    - Manages products
    - Manages Employees
    - Manages Clients
    - Broadcasts Message

2. EMPLOYEE
    - Manages product inventory
    - Manages user issue tickets
    - Manages user cart

3. CLIENT
    - View products in inventory
    - Checkout cart
    - Issue support tickets

# Introduction 
Using the information below, create an authentication API for a supermarket with the roles A, B & C.
 
- SUPERVISOR    (A)
- EMPLOYEE      (B)
- CLIENT        (C)

# Required Tools
1. Any Operating System (ie. MacOS X, Linux, Windows)
2. Visual Studio Code
3. Knowledge of Node Js
4. Knowledge of MongoDB

# Getting Started
TODO: How to use:
1.	Installation process:
    - Git clone the repository into VS Code
    - Run npm install
    - Run nodemon
    
2.	Project dependencies:
    - express
    - consola
    - body-parser
    - jsonwebtoken
    - passport
    - passport-jwt
    - cors
    - dotenv

3.	Latest releases
    - None

4.	Dev references
    - [NodeJS](https://nodejs.org/en/)
    - [MongoDB](https://www.mongodb.com/)
    - [VSCode](https://code.visualstudio.com/download)
    - [Heroku](https://dashboard.heroku.com/)

5.	API Documentation
    - https://documenter.getpostman.com/view/7190244/Tzm3pJCU

6. Deployment URL
    - https://node-rbac-api.herokuapp.com/




# Contribute
1. Git clone the repository into VS Code



