const router = require("express").Router();

/**
 * Import Authentication Exports
 */
const {
    userAuth,
    userLogin,
    checkRole,
    userRegister,
    serializeUser
} = require("../utils/auth");

//"client", "employee", "supervisor"

//Signup Route
router.post("/register-user", async (req, res) => {
    console.log(req.body.role);
    await userRegister(req.body, req.body.role, res);
});


//Login Route
router.post("/login-user", async (req, res) => {
    await userLogin(req.body, req.body.role, res);
});


// Logged In User Profile
router.get("/profile", userAuth, async (req, res) => {
    return res.json(serializeUser(req.user));
});

// Client Protected Route
router.get(
    "/client-only-route",
    userAuth,
    checkRole(["client"]),
    async (req, res) => {
        return res.json("Hello Client");
    }
);

// Employee Protected Route
router.get(
    "/employee-only-route",
    userAuth,
    checkRole(["employee"]),
    async (req, res) => {
        return res.json("Hello Employee");
    }
);

// Supervisor only Route
router.get(
    "/supervisor-only-route",
    userAuth,
    checkRole(["employee"]),
    async (req, res) => {
        return res.json("Hello Supervisor");
    }
);

// Supervisor & Employee only Route
router.get(
    "/supervisor-employee-route",
    userAuth,
    checkRole(["supervisor", "employee"]),
    async (req, res) => {
        return res.json("Hello Supervisor or Employee");
    }
);



module.exports = router;